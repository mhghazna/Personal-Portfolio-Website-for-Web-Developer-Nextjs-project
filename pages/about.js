'use client'
import React from "react";
import Head from "next/head";
import home from "../styles/Home.module.css";
import Sidebar from "@/components/Sidebar";
import Link from "next/link";
import {MdMore} from 'react-icons/md'


const about = ({ theme, closeSidebar }) => {
  return (
    <>
      <Head>
        <title>About us || ghazna.com </title>
        <meta
          name="keyword"
          content="ghazna.com, ghazna, ghazna portfolio, professional, personal portfolio, muhammad hasnain, hasnain, About us , ghazna.com/about"
        />
        <meta
          name="description"
          content="Ghazna.com About: To see the stories and the idea of this project "
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="png" href="/fav.png" sizes="200x100" />
      </Head>

      <main
        className={home.main}
        style={{
          backgroundColor: theme,
          color: theme == "black" ? "white" : "black",
        }}
      >
        {/* this page coding starting from here  */}
        <div className={home.intro}>
          <div className="page_section">
            <h1>About Me </h1>
            <div className={"container"}>
              <div className="content">
                <h2>Who am i ?</h2>
                <h3>Know me before hire me </h3>
                <p>
                  Hello there, im <Link href={'/'}>Muhammad Hasnain</Link> a professionate front end and backend <span>web developer </span> with over a 2 years of experience in HTML,CSS, Tailwind, Bootstrap, React JS and Full Control on Nodejs Backend 2023. I also enjoy doing reasearch to stay up-to-date with the latest trends and technologies in web development. I specifilize in creating modern, responsive, and user friednly websites and web applications that provides a seamiess experience to users. 
                </p>
                <div className="buttons">
                  <button>Know More <MdMore/></button>
                  <button>Get Resume </button>
                </div>
              </div>
              <img src="/my.jpg" alt="my personal picture" />
            </div>
          </div>

          {/* this page coding ending   */}
        </div>

        {/* sidebar  */}
        <Sidebar closeSidebar={closeSidebar} />
      </main>
    </>
  );
};

export default about;
