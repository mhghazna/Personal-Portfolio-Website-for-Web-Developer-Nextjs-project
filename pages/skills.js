'use client'
import React from 'react'
import Head from 'next/head'
import home from '../styles/Home.module.css'
import Sidebar from '@/components/Sidebar'

const services = ({theme, closeSidebar}) => {
    return (<>
        <Head>
            <title>My Skills  || ghazna.com </title>
            <meta name="keyword" content="ghazna.com, ghazna, ghazna portfolio, professional, personal portfolio, muhammad hasnain, hasnain, Skills , ghazna.com/skills " />
            <meta name="description" content="Ghazna.com services : Ny skills with represents with best and modern ui charts and loading bars " />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" type="png" href="/fav.png" sizes="200x100" />
        </Head>

        <main
        className={home.main}
        style={{
          backgroundColor: theme,
          color: theme == "black" ? "white" : "black",
        }}
      >

        {/* this page coding starting from here  */}
        <div className={home.intro}>
        
        <div className="page_section">


        <h1>My Skills</h1>
        <p>Here is my skills that allows to identify my expertise for your project.</p>
    

            <div className='container'>

                <div className="side-skills designs"></div>
                <div className="side-skills languages"></div>


            </div>


        </div>


        {/* this page coding ending   */}
        </div>

        {/* sidebar  */}
        <Sidebar closeSidebar={closeSidebar} />
      </main>


    </>
    )
}

export default services