'use client'
import Footer from '@/components/Footer';
import Navbar from '@/components/Navbar';
import '@/styles/globals.css'
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';


import LoadingBar from 'react-top-loading-bar'





export default function App({ Component, pageProps }) {
  const router = useRouter()
  const [contextval, setcontextval] = useState(router.pathname);
  // progressbar state 
  const [progress, setProgress] = useState(0)

  // color = #f7e2f7
  const [theme, setTheme] = useState("white");

 
  useEffect(() => {
    router.events.on("routeChangeStart", ()=>{
      setProgress(40)
    })
   
    router.events.on("routeChangeComplete", ()=>{
      setProgress(100)
    })
    setcontextval(router.pathname);
  }, [router.reload]);




  const toggleTheme = (e) => {
    setTheme(e.target.id);
    console.log("you are clicked on " + e.target.id);
  };

  const toggleSidebar = (e) => {
    const sidebar = document.getElementById("sidebar")
    sidebar.style.left = '0px'
  };

  const closeSidebar = (e) => {
    const sidebar = document.getElementById("sidebar")
    sidebar.style.left = '-300px'
  };


  return <>
    <LoadingBar
      color='#5a185a'
      progress={progress}
      waitingTime={600} 
      height={3}
      onLoaderFinished={() => setProgress(0)}
    />
    <Navbar toggleTheme={toggleTheme} theme={theme} toggleSidebar={toggleSidebar} contextval={contextval} />
    <Component {...pageProps} theme={theme} toggleTheme={toggleTheme} toggleSidebar={toggleSidebar} closeSidebar={closeSidebar} />
    <Footer />
  </>

} 

