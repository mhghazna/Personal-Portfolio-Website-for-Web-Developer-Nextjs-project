'use client'
import React from 'react'
import Head from 'next/head'
import home from '../styles/Home.module.css'
import Sidebar from '@/components/Sidebar'

const contact = ({theme, closeSidebar}) => {
    return (<>
        <Head>
            <title>Contact us  || ghazna.com </title>
            <meta name="keyword" content="ghazna.com, ghazna, ghazna portfolio, professional, personal portfolio, muhammad hasnain, hasnain, Contact us , ghazna.com/contact" />
            <meta name="description" content="Ghazna.com Contact: Contact with me on the contact us form or using social media accounts links avaliable just click now  " />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" type="png" href="/fav.png" sizes="200x100" />
        </Head>

        <main
        className={home.main}
        style={{
          backgroundColor: theme,
          color: theme == "black" ? "white" : "black",
        }}
      >

        {/* this page coding starting from here  */}
        <div className={home.intro}>
        
        <div className="page_section">


        <h1>My contact page  </h1>


            
        </div>


        {/* this page coding ending   */}
        </div>

        {/* sidebar  */}
        <Sidebar closeSidebar={closeSidebar} />
      </main>


    </>
    )
}

export default contact