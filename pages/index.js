'use client'
import Head from "next/head";
import Link from "next/link";
import home from "../styles/Home.module.css";
import {FaFacebook,FaGithub,FaLinkedin} from 'react-icons/fa'
import {MdMore} from 'react-icons/md'

import Sidebar from "@/components/Sidebar";

export default function Home({ theme, closeSidebar }) {
  return (
    <>
      <Head>
        <title>Welcome - HOME || ghazna.com </title>
        <meta name="keyword" content="ghazna.com, ghazna, ghazna portfolio, professional, personal portfolio, muhammad hasnain, hasnain" />
        <meta name="description" content="Hi My name is Muhammad hasnain ghazna And this is my personal portfolio website here you can get all about your web development learning way so please visit and sigup to get best and better projects, courses and much more thanks " />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="png" href="/fav.png" sizes="200x100" />
      </Head>

      <main
        className={home.main}
        style={{
          backgroundColor: theme,
          color: theme == "black" ? "white" : "black",
        }}
      >
        <div className={home.intro}>
          <div className={`${home.side} left`}>
            <h1 className="hed">
              STAY SCOLL
            </h1>
            <h1 className="hed2">
              welcome to <span>Ghazna Technical </span>
            </h1>

            <h2 style={{ marginTop: '1.5rem' }}>
              Hi iam <br /> <span className="span">MUHAMMAD HASNAIN GHAZNA  </span>
            </h2>

            <h3>Full Stack Web <span className="span">Developer </span></h3>
            
            <div className="buttons">
            <button>Learn More <MdMore/> </button>
            <button>Hire Me  </button>
            </div>
            <div className="icons" style={{marginLeft:"20px", marginTop:'1rem'}}>
          <Link href={"https://facebook.com/ghazna.gh"}><FaGithub /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaFacebook /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaLinkedin /></Link>
        </div>



          </div>

          <div className={`${home.side} right`}>
            <img src="/my.jpg" alt="my personal picture ghazna.com " />
          </div>
        </div>

        {/* sidebar  */}
        <Sidebar closeSidebar={closeSidebar} />
      </main>

    </>
  );
}
