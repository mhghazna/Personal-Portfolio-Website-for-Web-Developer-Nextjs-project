'use client'
import Link from "next/link";
import React from "react";
import {FaFacebook, FaTwitter,FaInstagram,FaSquareWhatsapp,FaGithub,FaLinkedin, FaWalking} from 'react-icons/fa'


const Footer = () => {
  return (
    <>
      <style jsx >
        {`
          footer {
            width: 100%;
            height: auto;
            padding: 10px;
            background-color: #f7e2f7;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }
          footer p {
            font-size: 1.3rem;
            font-family: "Barlow", sans-serif;
            text-align:center;
          }
          footer p a {
            text-decoration: none;
          }
         
        `}
      </style>

          <br />
          <br />
          <br />
      <footer>
        <p>
          CopyRight&copy; All Right Reserved <Link href={"/"}>Ghazna.com </Link>
          version
          <span>1.0.0</span> 2023
        </p>
        <div className="icons">
          <Link href={"https://facebook.com/ghazna.gh"}><FaFacebook /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaTwitter /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaInstagram /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaLinkedin /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaWalking /></Link>
          <Link href={"https://facebook.com/ghazna.gh"}><FaGithub /></Link>
        </div>
      </footer>
    </>
  );
};

export default Footer;
