'use client'
import header from "../styles/Header.module.css";

import { HiMenuAlt2 } from "react-icons/hi";
const Navbar = ({ toggleTheme, theme, toggleSidebar,contextval }) => {


  return (
    <div
      className={header.header}
      style={{
        backgroundColor: "#f7e2f7",
        color: theme == "black" ? "white" : "black",
      }}
    >
      <div className={header.headerTop}>
        <div clasname={header.sidebarbtn}>
          <button id="sidebarBtn" onClick={toggleSidebar}>
            <HiMenuAlt2 className="open" />
          </button>
        </div>
        <div className={header.logo}>
          <img src="/fav.png" alt="ghazna.com logo of website " />
        </div>
        <div className={header.themeBtns}>
          <div className="circle" id="orangered" onClick={toggleTheme}></div>
          <div className="circle" id="purple" onClick={toggleTheme}></div>
          <div
            className="circle"
            id={`${theme == "black" ? "white" : "black"}`}
            onClick={toggleTheme}
            style={{
              backgroundColor: theme == "white" ? "black" : "white",
              boxShadow: `0px 0px 1px 5px ${theme == "white" ? "black" : "white"
                } `,
            }}
          ></div>
        </div>
      </div>
      <div
        className={header.headerBottom}
        style={{
          backgroundColor: theme,
          color: theme == "black" ? "white" : "black",
        }}
      >
        <code>{contextval}</code>
      </div>
    </div>
  );
};

export default Navbar;
