import React, { useRef } from 'react'
import Link from 'next/link';
import sidebarcss from '../styles/sidebar.module.css'


'use client'
import { AiFillCloseCircle } from "react-icons/ai";
import {FaServicestack, FaHome} from 'react-icons/fa'
import {RiGalleryFill} from 'react-icons/ri'
import {GiSkills,GiNetworkBars} from 'react-icons/gi'
import {BsFillPhoneFill} from 'react-icons/bs'
import { FaMandalorian } from 'react-icons/fa';


const Sidebar = ({closeSidebar}) => {
    const ref = useRef()
    return (
        <div className={sidebarcss.sidebar} id="sidebar" ref={ref}>
            <div className={sidebarcss.close}>
                <AiFillCloseCircle className={sidebarcss.icon} onClick={closeSidebar} />
            </div>
            <div className="sidebarBody">
                <Link href='/' ><FaHome /> Home</Link>
                <Link href='/about' ><FaMandalorian /> About Me </Link>
                <Link href='/skills' ><FaMandalorian /> My Skills </Link>
                <Link href='/services' ><FaServicestack /> Services</Link>
                <Link href='/gallery' ><RiGalleryFill /> Gallery</Link>
                <Link href='/projects' ><GiNetworkBars /> Projects</Link>
                <Link href='/cv' ><GiSkills /> Curiculm Vite</Link>
                <Link href='/contact' ><BsFillPhoneFill /> Contact us </Link>
            </div>
        </div>
    )
}

export default Sidebar